using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjetoDonation.Models;

namespace Doador_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DoadorController : ControllerBase
    {
        private readonly ApiDbContext _context;

        public DoadorController(ApiDbContext context)
        {
            _context = context;
        }

        // GET: api/Doador
        [HttpGet]
        public ActionResult<IEnumerable<Doador>> Get()
        {
            return _context.Doador;
        }

        // GET: api/Doador/5
        [HttpGet("{id}")]
        public ActionResult<Doador> GetById(int id)
        {
            var item = _context.Doador.Find(id);

            if (item == null)
            {
                return NotFound();
            }

            return item;
        }

        // POST: api/Doador
        [HttpPost]
        public ActionResult<Doador> Post(Doador item)
        {
            _context.Doador.Add(item);
            _context.SaveChanges();

            return CreatedAtAction(nameof(GetById), new { id = item.Id }, item);
        }

        // PUT: api/Doador/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, Doador item)
        {
            if (id != item.Id)
            {
                return BadRequest();
            }

            _context.Entry(item).State = EntityState.Modified;
            _context.SaveChanges();

            return NoContent();
        }

        // DELETE: api/Doador/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var item = _context.Doador.Find(id);

            if (item == null)
            {
                return NotFound();
            }

            _context.Doador.Remove(item);
            _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
