using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjetoDonation.Models;

namespace Donatario_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DonatarioController : ControllerBase
    {
        private readonly ApiDbContext _context;

        public DonatarioController(ApiDbContext context)
        {
            _context = context;
        }

        // GET: api/Donatario
        [HttpGet]
        public ActionResult<IEnumerable<Donatario>> Get()
        {
            return _context.Donatario;
        }

        // GET: api/Donatario/5
        [HttpGet("{id}")]
        public ActionResult<Donatario> GetById(int id)
        {
            var item = _context.Donatario.Find(id);

            if (item == null)
            {
                return NotFound();
            }

            return item;
        }

        // POST: api/Donatario
        [HttpPost]
        public ActionResult<Donatario> Post(Donatario item)
        {
            _context.Donatario.Add(item);
            _context.SaveChanges();

            return CreatedAtAction(nameof(GetById), new { id = item.Id }, item);
        }

        // PUT: api/Donatario/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, Donatario item)
        {
            if (id != item.Id)
            {
                return BadRequest();
            }

            _context.Entry(item).State = EntityState.Modified;
            _context.SaveChanges();

            return NoContent();
        }

        // DELETE: api/Donatario/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var item = _context.Donatario.Find(id);

            if (item == null)
            {
                return NotFound();
            }

            _context.Donatario.Remove(item);
            _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
