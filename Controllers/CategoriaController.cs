using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjetoDonation.Models;

namespace Categoria_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriaController : ControllerBase
    {
        private readonly ApiDbContext _context;

        public CategoriaController(ApiDbContext context)
        {
            _context = context;
        }

        // GET: api/Categoria
        [HttpGet]
        public ActionResult<IEnumerable<Categoria>> Get()
        {
            return _context.Categoria;
        }

        // GET: api/Categoria/5
        [HttpGet("{id}")]
        public ActionResult<Categoria> GetById(int id)
        {
            var item = _context.Categoria.Find(id);

            if (item == null)
            {
                return NotFound();
            }

            return item;
        }

        // POST: api/Categoria
        [HttpPost]
        public ActionResult<Categoria> Post(Categoria item)
        {
            _context.Categoria.Add(item);
            _context.SaveChanges();

            return CreatedAtAction(nameof(GetById), new { id = item.Id }, item);
        }

        // PUT: api/Categoria/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, Categoria item)
        {
            if (id != item.Id)
            {
                return BadRequest();
            }

            _context.Entry(item).State = EntityState.Modified;
            _context.SaveChanges();

            return NoContent();
        }

        // DELETE: api/Categoria/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var item = _context.Categoria.Find(id);

            if (item == null)
            {
                return NotFound();
            }

            _context.Categoria.Remove(item);
            _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
