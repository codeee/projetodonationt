using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjetoDonation.Models;

namespace Produto_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {
        private readonly ApiDbContext _context;

        public ProdutoController(ApiDbContext context)
        {
            _context = context;
        }

        // GET: api/Produto
        [HttpGet]
        public ActionResult<IEnumerable<Produto>> Get()
        {
            return _context.Produto;
        }

        // GET: api/Produto/5
        [HttpGet("{id}")]
        public ActionResult<Produto> GetById(int id)
        {
            var item = _context.Produto.Find(id);

            if (item == null)
            {
                return NotFound();
            }

            return item;
        }

        // POST: api/Produto
        [HttpPost]
        public ActionResult<Produto> Post(Produto item)
        {
            _context.Produto.Add(item);
            _context.SaveChanges();

            return CreatedAtAction(nameof(GetById), new { id = item.Id }, item);
        }

        // PUT: api/Produto/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, Produto item)
        {
            if (id != item.Id)
            {
                return BadRequest();
            }

            _context.Entry(item).State = EntityState.Modified;
            _context.SaveChanges();

            return NoContent();
        }

        // DELETE: api/Produto/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var item = _context.Produto.Find(id);

            if (item == null)
            {
                return NotFound();
            }

            _context.Produto.Remove(item);
            _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
