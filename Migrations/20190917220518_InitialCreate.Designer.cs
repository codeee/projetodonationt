﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using ProjetoDonation.Models;

namespace ProjetoDonation.Migrations
{
    [DbContext(typeof(ApiDbContext))]
    [Migration("20190917220518_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("ProjetoDonation.Models.Categoria", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Nome");

                    b.HasKey("Id");

                    b.ToTable("Categoria");
                });

            modelBuilder.Entity("ProjetoDonation.Models.Doador", b =>
                {
                    b.Property<int>("DoadorId")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("Cpf");

                    b.Property<string>("Nome");

                    b.Property<string>("Telefone");

                    b.HasKey("DoadorId");

                    b.ToTable("Doador");
                });

            modelBuilder.Entity("ProjetoDonation.Models.Donatario", b =>
                {
                    b.Property<int>("DonatarioId")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("Cpf");

                    b.Property<string>("Nome");

                    b.Property<string>("Telefone");

                    b.HasKey("DonatarioId");

                    b.ToTable("Donatario");
                });

            modelBuilder.Entity("ProjetoDonation.Models.Produto", b =>
                {
                    b.Property<int>("ProdutoId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Descricao");

                    b.Property<int?>("DoadorId");

                    b.Property<int?>("DonatarioId");

                    b.Property<string>("Nome");

                    b.Property<string>("Tipo");

                    b.Property<int?>("categoriaId");

                    b.Property<string>("status");

                    b.HasKey("ProdutoId");

                    b.HasIndex("DoadorId");

                    b.HasIndex("DonatarioId");

                    b.HasIndex("categoriaId");

                    b.ToTable("Produto");
                });

            modelBuilder.Entity("ProjetoDonation.Models.Produto", b =>
                {
                    b.HasOne("ProjetoDonation.Models.Doador", "Doador")
                        .WithMany("Produtos")
                        .HasForeignKey("DoadorId");

                    b.HasOne("ProjetoDonation.Models.Donatario", "Donatario")
                        .WithMany("Produtos")
                        .HasForeignKey("DonatarioId");

                    b.HasOne("ProjetoDonation.Models.Categoria", "categoria")
                        .WithMany("Produtos")
                        .HasForeignKey("categoriaId");
                });
#pragma warning restore 612, 618
        }
    }
}
