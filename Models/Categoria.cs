using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ProjetoDonation.Models
{
    /// <summary>
    /// Classe resposável pelos dados do objeto categoria
    /// </summary>
    public class Categoria
    {
        public int Id { get; set; }
        public string Nome { get; set; }

        [DataMember]
        public virtual ICollection<Produto> Produtos { get; set; }

    }

}