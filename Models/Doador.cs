using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ProjetoDonation.Models
{
    /// <summary>
    /// Classe resposável pelos dados do objeto categoria
    /// </summary>

    public class Doador
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public long Cpf { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public string Endereco { get; set; }

        [DataMember]
        public virtual ICollection<Produto> Produtos { get; set; }
    }

}