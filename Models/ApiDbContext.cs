using Microsoft.EntityFrameworkCore;

namespace ProjetoDonation.Models
{
    public class ApiDbContext : DbContext
    {
        public ApiDbContext(DbContextOptions<ApiDbContext> options)
            : base(options)
        { }

        public DbSet<Donatario> Donatario { get; set; }
        public DbSet<Doador> Doador { get; set; }
        public DbSet<Produto> Produto { get; set; }
        public DbSet<Categoria> Categoria { get; set; }
    }


}
