using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ProjetoDonation.Models
{
    /// <summary>
    /// Classe resposável pelos dados do objeto categoria
    /// </summary>

    [DataContract(IsReference = true)]
    public class Produto
    {

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int IdDoador { get; set; }
        [DataMember]
        public int IdDonatario { get; set; }
        [DataMember]
        public int IdCategoria { get; set; }
        [DataMember]
        public string Nome { get; set; }
        [DataMember]
        public string Tipo { get; set; }
        [DataMember]
        public string Descricao { get; set; }
        [DataMember]
        [JsonConverter(typeof(StringEnumConverter))]
        public Status Status { get; set; }

        //public Foto
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual Doador Doador { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual Donatario Donatario { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual Categoria Categoria { get; set; }
    }

    public enum Status
    {
        DISPONÍVEL,
        INDISPONÍVEL,
    }

}