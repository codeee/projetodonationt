import React from 'react';
import { Container, Nav, Navbar, NavDropdown, Form, Button, FormControl } from 'react-bootstrap';
import { BrowserRouter, Route, Switch, Link } from 'react-router-dom';
import './App.css';
import { Home } from './components/Home.js';
import { Tarefas } from './components/Tarefas';
import { Categoria } from './components/Categoria';
import { Cadastro } from "./components/Cadastro";
import { Login } from "./components/Login";
import { Doacao } from "./components/Doacao";

function App() {
  return (
    <Container>
      <BrowserRouter>
        <Navbar className="navbar-expand-sm navbar-toggleable-sm ng-white border-bottom mb-3" bg="light" variant="light" expand="lg">
          <Navbar.Brand as={Link} to="/">Donation</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link as={Link} to="/">Home</Nav.Link>
              <NavDropdown title='Categoria' id='basic-nav-dropdown'  disabled>
                <NavDropdown.Item as={Link} to='/categoria'>categoria 1</NavDropdown.Item>
              </NavDropdown>
              <Nav.Link as={Link} to="/doacao">Doe Aqui</Nav.Link>
              {/*<NavDropdown title='Cadastros' id='basic-nav-dropdown'>
                <NavDropdown.Item as={Link} to='/tarefas'>Tarefas</NavDropdown.Item>
                </NavDropdown>*/}
            </Nav>
            <Form inline>
              <FormControl type="text" placeholder="Produto" className="mr-sm-2" id="pesquisa-produto" />
              <Button variant="outline-secondary">Procurar</Button>
            </Form>
            <Form>
              <Button variant="outline-secondary" as={Link} to='/login'>Login</Button>
              {/*<Button variant="outline-success" as={Link} to='/cadastro'>Cadastrar</Button>*/}
            </Form>
          </Navbar.Collapse>
        </Navbar>
        <Switch>
          <Route path="/" exact={true} component={Home} />
          <Route path="/tarefas" component={Tarefas} />
          <Route path="/categoria" component={Categoria} />
          <Route path="/cadastro" component={Cadastro} />
          <Route path="/login" component={Login} />
          <Route path="/doacao" component={Doacao} />
        </Switch>
      </BrowserRouter>
      <div className="footer-copyright text-left py-3">
        <hr/>
        <footer>
          <p>&copy; {new Date().getFullYear()} Donation</p>
        </footer>
      </div>
    </Container>
  );
}

export default App;