import 'bootstrap/dist/css/bootstrap.min.css';
import React, { Component } from 'react';
import { Container, Form, Button, Col, Card, type, FormGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
import "./Cadastro.css"
import { string } from 'prop-types';

export class Cadastro extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            endereco: '',
            nome: '',

        };
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleNomeChange = this.handleNomeChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this)
        this.handleEnderecoChange = this.handleEnderecoChange.bind(this)
        this.handleCpfChange = this.handleCpfChange.bind(this);
        this.handleCepChange = this.handleCepChange.bind(this);
    }

    handleEmailChange(e) {
        this.setState({
            email: e.target.value
        });
    }
    handleNomeChange(e) {
        this.setState({
            nome: e.target.value
        });
    }
    handleCpfChange(e) {
        this.setState({
            cpf: e.target.value
        });

    }
    handleEnderecoChange(e) {
        this.setState({
            endereco: e.target.value
        });
    }
    handlePasswordChange(e) {
        this.setState({
            password: e.target.value
        });
    }
    handleCepChange(e) {
        this.setState({
            cep: e.target.value
        });
    }

    valida() {
        if (document.cadastroValida.cpf.validity.patternMismatch) {
            if (document.cadastroValida.cpf.value == " ")
                alert("O CPF está incorreto");
        } else {
            alert("O CPF está correto");
        }
        return false;
    }

    render() {
        return (
            <Container>
                <Card body className="card-cadastro box-shadow">
                    <div>
                        <h2 className="text-cadastro">Cadastro</h2>
                    </div>
                    <Form name="cadastroValida" >
                        <Form.Group controlId="formGridNome">
                            <Form.Control type="text" placeholder="Nome" value={this.state.nome} onChange={this.handleNomeChange} />
                        </Form.Group>

                        <Form.Group >
                            <Form.Control on type="text" name="cpf" pattern="\d{3}\.\d{3}\.\d{3}-\d{2}" title="Digite o CPF no formato XXX.XXX.XXX-XX" maxlength="14" placeholder="CPF "
                                onSubmit="return valida()"></Form.Control>
                        </Form.Group>

                        <Form.Row>
                            <Form.Group as={Col} controlId="formGridEmail">
                                <Form.Control type="email" placeholder="Email" value={this.state.email} onChange={this.handleEmailChange} />
                            </Form.Group>

                            <Form.Group as={Col} controlId="formGridPassword" >
                                <Form.Control type="password" placeholder="Password" value={this.state.password} onChange={this.handlePasswordChange} />
                            </Form.Group>
                        </Form.Row>

                        <Form.Group controlId="formGridAddress1">
                            <Form.Control placeholder="Endereço" value={this.state.endereco} onChange={this.handleEnderecoChange} />
                        </Form.Group>

                        <Form.Row>
                            <Form.Group as={Col} controlId="formGridZip">
                                <Form.Control placeholder="CEP" value={this.state.cep} onChange={this.handleCepChange} />
                            </Form.Group>

                            <Form.Group as={Col} controlId="formGridCity">
                                <Form.Control placeholder="Cidade" value={this.state.cidade} onChange={this.handleCidadeChange} />
                            </Form.Group>

                            <Form.Group as={Col} controlId="formGridState">
                                <Form.Control as="select">
                                    <option>Estado</option>
                                    <option>...</option>
                                </Form.Control>
                            </Form.Group>
                        </Form.Row>
                        <Button variant="outline-secondary" type="submit">
                            Cadastrar
                        </Button>
                    </Form>
                </Card>
            </Container >
        );
    }
}
