import React, { Component } from 'react';
import { Container, Form, Button, Card } from "react-bootstrap";
import { Link, Switch, Route } from 'react-router-dom';
import './Login.css';
import { Cadastro } from "./Cadastro";

export class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            descricao: '',
            prioridade: 0
        };

    }

    alertClicked() {
        alert('You clicked the third ListGroupItem');
    }


    render() {
        return (
            <Container>
                <Card body className="box-shadow card-login">
                    <div>
                        <h2>Donation</h2>
                        <h2 id="text" class="text-muted">
                            Doe fácil, receba mais fácil ainda
                        </h2>
                        {/*<h2>Login</h2>*/}
                    </div>

                    <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Control type="email" placeholder="Email" />
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Control type="password" placeholder="Senha" />
                        </Form.Group>
                        <Form.Group controlId="formBasicCheckbox">
                            <Form.Check type="checkbox" label="Lembrar-me" />
                        </Form.Group>
                        <Form.Group inline>
                            <Button variant="outline-secondary" type="submit" className="mr-sm-2">
                                Entrar
                            </Button>
                            <Button variant="outline-secondary" as={Link} to='/cadastro'>
                                Cadastrar-se
                            </Button>
                        </Form.Group>
                    </Form>
                </Card>
                <Switch>
                    <Route path="/cadastro" component={Cadastro} />
                </Switch>
            </Container>
        );
    }
}