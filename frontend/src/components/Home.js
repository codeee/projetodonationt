import React, { Component } from 'react';
import { Container, Card, Form, Button, FormControl } from "react-bootstrap";
import { Link } from 'react-router-dom';
import "./Home.css";
import "./img/facebook.png";
import imagemEscolhida from './img/bicicleta1.jpg';
import imagem2 from './img/cadeira-hertz-slim.jpg';
import imgOculos from './img/oculos.jpg';
import donation from './img/a.png';


export class Home extends Component {

  constructor(props) {
    super(props);
  }


  render() {
    return (
      <Container>
        {/* <Card body className="card-home box-shadow">
          <Form inline>
            <FormControl type="text" placeholder="Produto" className="mr-sm-2" id="pesquisa-produto" />
            <Button variant="outline-secondary">Procurar</Button>
          </Form>
        </Card> */}
        <Card className="card-home box-shadow">
          <img src={donation} alt="teste" class="img-thumbnail" id="aa" />
          <div class="carousel-caption d-none d-md-block">
            <h5>Através do Donation, você pode doar e receber produtos! Doe Aqui!</h5>
            <p>Doe fácil, receba mais fácil ainda</p>
          </div>
        </Card>

        {/* <Card body id="aa" className="card-home box-shadow">
          <h2>Top Doações</h2>
          <div class="container">
            <div class="row">
              <div class="col-md-4">
                <div class="card mb-4 shadow-sm" id="img-home">
                  <img src={imagemEscolhida} alt="teste" class="img-thumbnail" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="card mb-4 shadow-sm" id="img-home">
                  <img src={imagem2} alt="teste" class="img-thumbnail" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="card mb-4 shadow-sm" id="img-home">
                  <img src={imgOculos} alt="teste" class="img-thumbnail" />
                </div>
              </div>
            </div>
          </div>
        </Card> */}
        {/* <Card body className="card-home2 box-shadow "> 
          <div>
            <h2>Donation</h2>
          </div>
          <div class="align-center">
            <img src={imagemEscolhida} alt="teste" class="img-thumbnail div-image2 mr-sm-2" />
            <Form id="donation" >
              <Form.Group controlId="formBasicEmail" id="f">
                <Form.Control type="text" placeholder="Nome do Produto" class="input-nome" />
              </Form.Group>
              <Form.Group controlId="formBasicEmail" id="g">
                <Form.Control type="text" placeholder="Descrição" class="input-desc" />
              </Form.Group>
              <Form.Group id="h">
                <Button variant="outline-secondary" type="submit">
                  Publicar
              </Button>
              </Form.Group>
            </Form>
          </div>
        </Card> */}
      </Container>
    );
  }
}