import React, { Component } from 'react';
import { Form, Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";

export class Tarefas extends Component {

  constructor(props) {
    super(props);

    this.state = {
      descricao: '',
      prioridade: 0
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDescricaoChange = this.handleDescricaoChange.bind(this);
    this.handlePrioridadeChange = this.handlePrioridadeChange.bind(this);
  }

  handleSubmit(e) {
    alert('Uma tarefa foi enviada: ' + JSON.stringify(this.state));
    this.setState({
      descricao: '',
      prioridade: 0
    });
    e.preventDefault();
  }

  handleDescricaoChange(e) {
    this.setState({
      descricao: e.target.value
    });
  }

  handlePrioridadeChange(e) {
    this.setState({
      prioridade: e.target.value
    });
  }

  render() {
    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          <Form.Group>
            <Form.Label>Descrição</Form.Label>
            <Form.Control type='text' placeholder='Descrição da tarefa' value={this.state.descricao} onChange={this.handleDescricaoChange} />
          </Form.Group>
          <Form.Group>
            <Form.Label>Prioridade</Form.Label>
            <Form.Control type='number' placeholder='Prioridade' value={this.state.prioridade} onChange={this.handlePrioridadeChange} />
          </Form.Group>
          <Button variant='primary' type='submit'>
            Inserir
           </Button>
        </Form>
      </div>
    );
  }
}
