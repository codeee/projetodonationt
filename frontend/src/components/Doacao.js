import React, { Component } from 'react';
import { Container, Form, Button, Card } from "react-bootstrap";
import { Link } from 'react-router-dom';
import "./Doacao.css";
import "./Home.css";
import imagemEscolhida from './img/bicicleta1.jpg';
import { makeStyles } from '@material-ui/core/styles';

export class Doacao extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Container>
        <Card body id="cardis" className="box-shadow card-doacao">
          <div>
            <h2>Doe Aqui!</h2>
            <h2 id="text" class="text-muted">
              Doe fácil, receba mais fácil ainda
            </h2>
          </div>
          <Form id="form">
            {/* <img src={imagemEscolhida} alt="teste" class="img-thumbnail div-image2 mr-sm-2" /> */}
            <Form.Group controlId="formBasicEmail">
              <Form.Control type="text" placeholder="Nome do Produto" />
            </Form.Group>
            <Form.Group controlId="exampleForm.ControlTextarea1">
              <Form.Control as="textarea" rows="3" placeholder="Descrição"/>
            </Form.Group>
            <Form.Group>
              <Button variant="outline-secondary" type="submit" className="mr-sm-2">
                Publicar
              </Button>
              <Button variant="outline-secondary" type="submit" className="mr-sm-2">
                Adicionar Imagem
              </Button>
            </Form.Group>
          </Form>
        </Card>
      </Container>
    );
  }
}