import React, { Component } from 'react';
import { Container, Form, Button, ListGroup, Card } from "react-bootstrap";
import imagemEscolhida from './img/bicicleta1.jpg';

export class Categoria extends Component {

    handleSubmit(e) {
        alert('Uma tarefa foi enviada: ' + JSON.stringify(this.state));

        this.setState({
            descricao: '',
            prioridade: 0
        });
        e.preventDefault();
    }

    handleDescricaoChange(e) {
        this.setState({
            descricao: e.target.value
        });
    }

    handlePrioridadeChange(e) {
        this.setState({
            prioridade: e.target.value
        });
    }

    handleCategoriaChange(e) {
        this.setState({
            Categoria: e.target.value
        });
    }



    constructor(props) {
        super(props);

        this.state = {
            descricao: '',
            prioridade: 0
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDescricaoChange = this.handleDescricaoChange.bind(this);
        this.handlePrioridadeChange = this.handlePrioridadeChange.bind(this);
        this.handleCategoriaChange = this.handleCategoriaChange.bind(this);
    }

    render() {
        return (
            <Container>
                <Card body className="card-home box-shadow mr-sm-3">
                    <h2>Categoria 1</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card mb-4 shadow-sm">
                                    <img src={imagemEscolhida} alt="teste" class="img-thumbnail" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card mb-4 shadow-sm">
                                    <img src={imagemEscolhida} alt="teste" class="img-thumbnail" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card mb-4 shadow-sm">
                                    <img src={imagemEscolhida} alt="teste" class="img-thumbnail" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card mb-4 shadow-sm">
                                    <img src={imagemEscolhida} alt="teste" class="img-thumbnail" />
                                </div>
                            </div> <div class="col-md-4">
                                <div class="card mb-4 shadow-sm">
                                    <img src={imagemEscolhida} alt="teste" class="img-thumbnail" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card mb-4 shadow-sm">
                                    <img src={imagemEscolhida} alt="teste" class="img-thumbnail" />
                                </div>
                            </div>

                        </div>
                    </div>
                </Card>
            </Container>
        );
    }
}
